<?php include 'header.php'; ?>
<div class="content">
    <div class="container-fluid">
        <div class="row"> 
            <div class="col-md-12"> <!-- cost -->
                <div class="card">
                    <div class="content">
                        <div id="cost" style="width:100%; height:400px;"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-12"> <!-- b/w -->
                <div class="card">
                    <div class="content">
                        <div id="bw" style="width:100%; height:400px;"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-12"> <!-- color -->
                <div class="card">
                    <div class="content">
                        <div id="color" style="width:100%; height:400px;"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php include 'footer.php'; ?>
<style type="text/css">
    #cost {
        min-width: 310px;
        max-width: 800px;
        height: 400px;
        margin: 0 auto
    }
</style>
<script type="text/javascript">
    $(document).ready(function(){
        /*montly cost*/
        $.ajax({
            url: 'data.php',
            data: {tipe: 1},
            type: 'get',
            dataType: 'json'
        }).done(function (data)
        {
            create_chart('cost', 'Monthly Cost', data);
        });

        /*b/w usage*/
        $.ajax({
            url: 'data.php',
            data: {tipe: 2, jenis: '0'},
            type: 'get',
            dataType: 'json'
        }).done(function (data)
        {
            create_chart('bw', 'B/W Usage', data);
        });

        /*color usage*/
        $.ajax({
            url: 'data.php',
            data: {tipe: 2, jenis: '1'},
            type: 'get',
            dataType: 'json'
        }).done(function (data)
        {
            create_chart('color', 'Color Usage', data);
        });

        function create_chart(id, title, data) {
            Highcharts.chart(id, {
                title: {
                    text: title
                },
                xAxis: {
                    categories: data.x
                },
                yAxis: {
                    title: {
                        text: id
                    }
                },
                legend: {
                    layout: 'horizontal',
                    align: 'right',
                    verticalAlign: 'bottom'
                },

                series: [{
                    name: id,
                    data: data.y
                }]
            });
        }
    });
</script>