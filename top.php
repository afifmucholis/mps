<?php include 'header.php';
/*bulan*/ 
$q = "SELECT * FROM bulan";
$r = mysqli_query($conn, $q);
$bulan = '';
while($row = mysqli_fetch_array($r))
{
    $bulan .= "<option value='".$row['id']."'>".$row['bulan']."</option>";
}
/*tahun*/
$q = "SELECT distinct(tahun) as th FROM usage_printer";
$r = mysqli_query($conn, $q);
$tahun = '';
while($row = mysqli_fetch_array($r))
{
    $tahun .= "<option value='".$row['th']."'>".$row['th']."</option>";
}
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <div class="form-inline">
                          <div class="form-group">
                            <label> TOP USER IN     : </label>
                            <select class="form-control" name="bulan" id="bulan">
                                <?=$bulan?>
                            </select>
                            <select class="form-control form-xs" name="tahun" id="tahun">
                                <?=$tahun?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary" id="submit" >show</button>
                        <button type="submit" class="btn btn-default" id="print" disabled="true">print</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="DivIdToPrint">
        <div class="col-xs-6">
            <div class="card">
                <div class="header">
                    <h4 class="title">Top 20 User Color</h4>
                </div>
                <div class="content table-responsive">
                    <table class="table table-hover table-bordered " id="topColor" style="font-size: 20px">
                        <thead>
                            <th width="10%">ID</th>
                            <th width="70%">Name</th>
                            <th width="10%">Division</th>
                            <th width="10%">Sheets</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="card">
                <div class="header">
                    <h4 class="title">Top 20 User BW</h4>
                </div>
                <div class="content table-responsive">
                    <table class="table table-hover table-bordered " id="topBW" style="font-size: 20px">
                        <thead>
                            <th width="10%">ID</th>
                            <th width="70%">Name</th>
                            <th width="10%">Division</th>
                            <th width="10%">Sheets</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
</div>
<?php include 'footer.php'; ?>

<script type="text/javascript">
    $(document).ready(function(){
        $('#print').click(function() {
            printDiv();
        });
        $('#submit').click(function () {
            $('#print').prop('disabled', false);
            /*top BW*/
            $.ajax({
                url: 'data.php',
                data: { tipe: 3, 
                    type: '0', 
                    bulan: $('#bulan').val(), 
                    tahun: $('#tahun').val() },
                    type: 'get',
                    dataType: 'json'
                }).done(function (data)
                {
                    $('#topBW tbody').html(data.data);
                });

                /*top Color*/
                $.ajax({
                    url: 'data.php',
                    data: { tipe: 3, 
                        type: '1', 
                        bulan: $('#bulan').val(), 
                        tahun: $('#tahun').val()},
                        type: 'get',
                        dataType: 'json'
                    }).done(function (data)
                    {
                        $('#topColor tbody').html(data.data);

                    });
                });
        function printDiv() 
        {

          var divToPrint=document.getElementById('DivIdToPrint');

          var newWin=window.open('','Print-Window');
          var header = '<html><head><link href="assets/css/bootstrap.min.css" rel="stylesheet" /><link href="assets/css/print.css" rel="stylesheet" /></head>';
          newWin.document.open();

          newWin.document.write(header+'<body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

          newWin.document.close();

          // setTimeout(function(){newWin.close();},10);

      }
  });
</script>