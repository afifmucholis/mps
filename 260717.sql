/*
SQLyog Community v12.4.3 (64 bit)
MySQL - 10.1.21-MariaDB : Database - mps1
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mps1` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `mps1`;

/*Table structure for table `bulan` */

DROP TABLE IF EXISTS `bulan`;

CREATE TABLE `bulan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bulan` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `bulan` */

insert  into `bulan`(`id`,`bulan`) values 
(1,'Januari'),
(2,'Februari'),
(3,'Maret'),
(4,'April'),
(5,'Mei'),
(6,'Juni'),
(7,'Juli'),
(8,'Agustus'),
(9,'September'),
(10,'Oktober'),
(11,'November'),
(12,'Desember');

/*Table structure for table `cost_printer` */

DROP TABLE IF EXISTS `cost_printer`;

CREATE TABLE `cost_printer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_bulan` smallint(2) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `cost` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `cost_printer` */

insert  into `cost_printer`(`id`,`id_bulan`,`tahun`,`cost`) values 
(1,1,2016,10000),
(2,2,2016,7000),
(3,1,2017,110000),
(4,2,2017,115000);

/*Table structure for table `divisi` */

DROP TABLE IF EXISTS `divisi`;

CREATE TABLE `divisi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `divisi` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=latin1;

/*Data for the table `divisi` */

insert  into `divisi`(`id`,`divisi`) values 
(1,'ACC\r'),
(2,'ACC/BO\r'),
(3,'Assembling 1\r'),
(4,'ASSY\r'),
(5,'ASSY2\r'),
(6,'BoD\r'),
(7,'BODY\r'),
(8,'Body 2\r'),
(9,'Casting\r'),
(10,'CIT\r'),
(11,'CIT - BR2\r'),
(12,'CIT Infra\r'),
(13,'CKD\r'),
(14,'CORPL\r'),
(15,'Cost Control - PCD\r'),
(16,'Cost Cotrol - PCD\r'),
(17,'CSR\r'),
(18,'CSVC\r'),
(19,'Design Engineering\r'),
(20,'DOM\r'),
(21,'DOMM\r'),
(22,'DTMM\r'),
(23,'EA\r'),
(24,'EDER\r'),
(25,'EHS\r'),
(26,'EID\r'),
(27,'Engine\r'),
(28,'FIN\r'),
(29,'Finance\r'),
(30,'GA\r'),
(31,'General Accounting & Tax\r'),
(32,'HR/HRO 2\r'),
(33,'HRD\r'),
(34,'HRO\r'),
(35,'IARM\r'),
(36,'Infrastructure\r'),
(37,'IR\r'),
(38,'IRA\r'),
(39,'KAP\r'),
(40,'KEP\r'),
(41,'LOG\r'),
(42,'Log. Assy\r'),
(43,'Logistic - EID\r'),
(44,'Logistic Assy\r'),
(45,'Maintenance\r'),
(46,'Marketing Product Planning / Product I & II\r'),
(47,'MPP\r'),
(48,'MPPD\r'),
(49,'MTN\r'),
(50,'PAD\r'),
(51,'PAINT\r'),
(52,'Painting 3\r'),
(53,'PARTS\r'),
(54,'PCD\r'),
(55,'PCL\r'),
(56,'PE\r'),
(57,'PE / Design Engineering\r'),
(58,'PE / Project Cost Planning\r'),
(59,'PE Assy\r'),
(60,'PE Press\r'),
(61,'PPCL\r'),
(62,'PRESS\r'),
(63,'PRO\r'),
(64,'PROD\r'),
(65,'Produksi\r'),
(66,'Project Cost Planning\r'),
(67,'PTC\r'),
(68,'PuD\r'),
(69,'PURCHASING\r'),
(70,'QA\r'),
(71,'QC\r'),
(72,'QE\r'),
(73,'QE / QC\r'),
(74,'QI\r'),
(75,'QI - QC\r'),
(76,'Quality\r'),
(77,'R&D\r'),
(78,'RnD\r'),
(79,'RPM\r'),
(80,'SPD\r'),
(81,'Technical Service\r'),
(82,'TSD\r');

/*Table structure for table `printer` */

DROP TABLE IF EXISTS `printer`;

CREATE TABLE `printer` (
  `sn` varchar(20) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `lokasi` varchar(50) NOT NULL,
  PRIMARY KEY (`sn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `printer` */

insert  into `printer`(`sn`,`nama`,`lokasi`) values 
('140268','HO-Ckd-01','ckd'),
('140269','HO-Ckd-02','ckd');

/*Table structure for table `usage_printer` */

DROP TABLE IF EXISTS `usage_printer`;

CREATE TABLE `usage_printer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `id_bulan` int(11) NOT NULL,
  `tahun` int(4) NOT NULL,
  `sn_printer` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '0 = B/W, 1 = Color',
  `sheets` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`username`),
  KEY `id_bulan` (`id_bulan`),
  KEY `id_tahun` (`tahun`),
  KEY `id_type` (`type`),
  KEY `id_printer` (`sn_printer`),
  KEY `id_tahun_2` (`tahun`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `usage_printer` */

insert  into `usage_printer`(`id`,`username`,`id_bulan`,`tahun`,`sn_printer`,`type`,`sheets`) values 
(1,'afif.mucholis',1,2017,140268,1,100),
(2,'afif.mucholis',1,2017,140268,0,500),
(3,'afif.mucholis',2,2017,140268,0,600),
(4,'afif.mucholis',12,2016,140268,0,50),
(5,'afif.mucholis',1,2017,140269,1,2),
(6,'afif.mucholis',1,2017,140269,0,10000),
(7,'jhon.doe',1,2017,140269,0,1);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `username` varchar(50) NOT NULL,
  `nama` varchar(70) DEFAULT NULL,
  `divisi` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`username`),
  KEY `id_divisi` (`divisi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`username`,`nama`,`divisi`) values 
('afif.mucholis','Afif Mucholis','CIT'),
('jhon.doe','jhon doe','ACC');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
