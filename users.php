<?php include 'header.php'; ?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <div class="form-inline">
                            <div class="form-group">
                                <label> USERS     : </label>
                                <select class="form-control" name="users" id="users" style="width: 300px;">
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary" id="submit" style="height: 28px; padding: 2px, 4px">show</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12"> <!-- cost -->
                <div class="card">
                    <div class="content">
                        <div id="bw" style="width:100%; height:400px;"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12"> <!-- cost -->
                <div class="card">
                    <div class="content">
                        <div id="color" style="width:100%; height:400px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>
<style type="text/css">
    #cost {
        min-width: 310px;
        max-width: 800px;
        height: 400px;
        margin: 0 auto
    }
</style>
<script type="text/javascript">
    $(document).ready(function(){
        /*select2 users*/
        $("#users").select2({
            ajax: {
                url: "data.php?tipe=5",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, 
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, 
            minimumInputLength: 1
        });

        /*create grafik*/
        $('#submit').click(function() {
            $.ajax({
                url: 'data.php',
                data: {tipe: 6, username: $('#users').val()},
                type: 'get',
                dataType: 'json'
            }).done(function (data)
            {
                create_chart($('#users').text(), data);
                $('#users').text('');
            });
        });
        
        function create_chart(title, data) {
            Highcharts.chart('bw', {
                title: {
                    text: title+' B/W Usage'
                },
                xAxis: {
                    categories: data.x
                },
                yAxis: {
                    title: {
                        text: 'Sheets'
                    }
                },
                legend: {
                    layout: 'horizontal',
                    align: 'right',
                    verticalAlign: 'bottom'
                },

                series: [{
                    name: 'B/W',
                    data: data.y
                }]
            });

            Highcharts.chart('color', {
                title: {
                    text: title+' Color Usage'
                },
                xAxis: {
                    categories: data.x1
                },
                yAxis: {
                    title: {
                        text: 'Sheets'
                    }
                },
                legend: {
                    layout: 'horizontal',
                    align: 'right',
                    verticalAlign: 'bottom'
                },

                series: [{
                    name: 'Color',
                    data: data.y1
                }]
            });
        }
    });
</script>