<?php require_once 'koneksi.php'; ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>MPS Dashboard</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link href="assets/css/highcharts.css" rel="stylesheet"/>

    <!--     Fonts and icons     -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

    <!-- datatables -->
    <link href="assets/css/datatables.min.css" rel="stylesheet">

    <!-- select2 -->
    <link href="assets/css/select2.min.css" rel="stylesheet">
    
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="blue" >
        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text">
                    MPS Dashboard
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="index.php">
                        <i class="pe-7s-graph"></i>
                        <p>Overview</p>
                    </a>
                </li>
                <li>
                    <a href="top.php">
                        <i class="pe-7s-user"></i>
                        <p>Top 20 User</p>
                    </a>
                </li>
                <li>
                    <a href="printer.php">
                        <i class="pe-7s-print"></i>
                        <p>Usage/Printer</p>
                    </a>
                </li>
                <li>
                    <a href="users.php">
                        <i class="pe-7s-users"></i>
                        <p>Usage/User</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse">

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#">
                                <p>PT Astra Daihatsu Motor</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

