<?php
require 'koneksi.php';
$tipe = $_GET['tipe'];
switch ($tipe) {
	case '1': /*cost monthly*/
		$q = "SELECT 
				CONCAT(bulan,' ',tahun) AS months,
				cost
				FROM cost_printer
				JOIN bulan
				ON cost_printer.id_bulan = bulan.id";
		$r = mysqli_query($conn, $q);
		$x = array();
		$y = array();
		while($row = mysqli_fetch_array($r))
	    {
	        $x[] = $row['months'];
	        $y[] = intval($row['cost']);
	    }
	    $return['x'] = $x;
	    $return['y'] = $y;
	    echo json_encode($return);
	    break;
	case '2': /*usage BW and color*/
		$q = "SELECT 
			CONCAT(bulan,' ',tahun) AS months,
			sum(sheets) as sheets
			FROM usage_printer
			JOIN bulan
			ON usage_printer.id_bulan = bulan.id
			WHERE usage_printer.type = ".$_GET['jenis']."
			GROUP BY id_bulan, tahun
			ORDER BY tahun, id_bulan ASC";
		$r = mysqli_query($conn, $q);
		$x = array();
		$y = array();
		while($row = mysqli_fetch_array($r))
	    {
	        $x[] = $row['months'];
	        $y[] = intval($row['sheets']);
	    }
	    $return['x'] = $x;
	    $return['y'] = $y;
	    echo json_encode($return);
	    break;
	case '3': /*top 20 users*/
		$q = "SELECT
				user.nama,
				user.divisi,
				sum(usage_printer.sheets) as sheets
				FROM usage_printer
				JOIN mps1.user
				ON usage_printer.username = user.username
				WHERE
				usage_printer.tahun = ".$_GET['tahun']." AND
				usage_printer.id_bulan = ".$_GET['bulan']." AND
				usage_printer.type = ".$_GET['type']."
				GROUP BY user.username
				ORDER BY sheets DESC
				LIMIT 20";
		$r = mysqli_query($conn, $q);
		$data = '';
		$no = 1;
		if ($r->num_rows > 0) {
			while($row = mysqli_fetch_array($r))
		    {
		        $data .= '<tr><td>'.
		        			$no.'</td><td>'.
		        			$row['nama'].'</td><td>'.
		        			$row['divisi'].'</td><td>'.
		        			$row['sheets'].'</td></tr>';
		        $no++;
		    }
		}else{
	        $data .= '<tr><td colspan="4" align="center">--belum ada data--</td></tr>';
		}
	    $return['data'] = $data;
	    echo json_encode($return);
		break;
	case '4': /*printer usage*/
		$q = "SELECT
				printer.sn,
				printer.nama,
				printer.lokasi,
				SUM(usage_printer.sheets) AS bw
				FROM printer
				LEFT JOIN usage_printer
				ON printer.sn = usage_printer.sn_printer
				WHERE
				usage_printer.tahun = ".$_GET['tahun']." AND
				usage_printer.id_bulan = ".$_GET['bulan']." AND
				usage_printer.type = 0
				GROUP BY sn";
		$r = mysqli_query($conn, $q);
		$q2 = "	SELECT sn_printer AS sn, SUM(usage_printer.sheets) AS color
				FROM usage_printer
				WHERE 
				usage_printer.tahun = ".$_GET['tahun']." AND
				usage_printer.id_bulan = ".$_GET['bulan']." AND
				usage_printer.type = 1
				GROUP BY sn";
		$r2 = mysqli_query($conn, $q2);
		$data = '';
		$no = 1;
		$resultset = array();
		$resultset2 = array();

		while ($row = mysqli_fetch_array($r)) {
			$resultset[] = $row;
		}
		while ($row2 = mysqli_fetch_array($r2)) {
			$resultset2[] = $row2;
		}
		if ($r->num_rows > 0) {
		foreach ($resultset as $result){
			$color = 0;
			$total = $result['bw'];
			foreach ($resultset2 as $key) {
				if ($result['sn'] == $key['sn']) {
					$color = $key['color'];
					$total += $key['color'];
				}
			}
			$data .= '<tr><td>'.
		        			$no.'</td><td>'.
		        			$result['sn'].'</td><td>'.
		        			$result['nama'].'</td><td>'.
		        			$result['lokasi'].'</td><td>'.
		        			$result['bw'].'</td><td>'.
		        			$color.'</td><td>'.
		        			$total.'</td></tr>';
    			$no++;
		}
		}else{
	        $data .= '<tr><td colspan="7" align="center">--belum ada data--</td></tr>';
		}
	    $return['data'] = $data;
	    echo json_encode($return);
		break;
	case '5': /*select2 nama users*/
		$q = "	SELECT username AS id, nama AS text
				FROM user 
				WHERE nama LIKE '%".$_GET['q']."%'";
		$r = mysqli_query($conn, $q);
		$data = array();
		$resultset = array();
		$id = array();
		$text = array();
		
		while ($row = mysqli_fetch_array($r)) {
			$resultset[] = $row;
		}
		foreach ($resultset as $result){
			$data[] = array(
							'id' => $result['id'], 
							'text' => $result['text'] 
							);
		}
	    echo json_encode($data);
		break;
	case '6':
		$q = "SELECT 
				CONCAT(bulan,' ',tahun) AS months,
				SUM(sheets) AS sheets
				FROM usage_printer
				JOIN bulan
				ON usage_printer.id_bulan = bulan.id
				WHERE usage_printer.type = 0 AND
				usage_printer.username = '".$_GET['username']."'
				GROUP BY bulan, tahun
				ORDER BY tahun, id_bulan ASC";
		$r = mysqli_query($conn, $q);
		$x = array();
		$y = array();
		while($row = mysqli_fetch_array($r))
	    {
	        $x[] = $row['months'];
	        $y[] = intval($row['sheets']);
	    }
	    $return['x'] = $x;
	    $return['y'] = $y;

	    $q1 = "SELECT 
				CONCAT(bulan,' ',tahun) AS months,
				SUM(sheets) AS sheets
				FROM usage_printer
				JOIN bulan
				ON usage_printer.id_bulan = bulan.id
				WHERE usage_printer.type = 1 AND
				usage_printer.username = '".$_GET['username']."'
				GROUP BY bulan, tahun
				ORDER BY tahun, id_bulan ASC";
		$r1 = mysqli_query($conn, $q1);
		$x1 = array();
		$y1 = array();
		while($row1 = mysqli_fetch_array($r1))
	    {
	        $x1[] = $row1['months'];
	        $y1[] = intval($row1['sheets']);
	    }
	    $return['x1'] = $x1;
	    $return['y1'] = $y1;
	    echo json_encode($return);
		break;
}
?>