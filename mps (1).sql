-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 14, 2017 at 01:15 PM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mps`
--

-- --------------------------------------------------------

--
-- Table structure for table `bulan`
--

CREATE TABLE `bulan` (
  `id` int(11) NOT NULL,
  `bulan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bulan`
--

INSERT INTO `bulan` (`id`, `bulan`) VALUES
(1, 'Januari'),
(2, 'Februari'),
(3, 'Maret'),
(4, 'April'),
(5, 'Mei'),
(6, 'Juni'),
(7, 'Juli'),
(8, 'Agustus'),
(9, 'September'),
(10, 'Oktober'),
(11, 'November'),
(12, 'Desember');

-- --------------------------------------------------------

--
-- Table structure for table `divisi`
--

CREATE TABLE `divisi` (
  `id` int(11) NOT NULL,
  `divisi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `divisi`
--

INSERT INTO `divisi` (`id`, `divisi`) VALUES
(1, 'ACC\r'),
(2, 'ACC/BO\r'),
(3, 'Assembling 1\r'),
(4, 'ASSY\r'),
(5, 'ASSY2\r'),
(6, 'BoD\r'),
(7, 'BODY\r'),
(8, 'Body 2\r'),
(9, 'Casting\r'),
(10, 'CIT\r'),
(11, 'CIT - BR2\r'),
(12, 'CIT Infra\r'),
(13, 'CKD\r'),
(14, 'CORPL\r'),
(15, 'Cost Control - PCD\r'),
(16, 'Cost Cotrol - PCD\r'),
(17, 'CSR\r'),
(18, 'CSVC\r'),
(19, 'Design Engineering\r'),
(20, 'DOM\r'),
(21, 'DOMM\r'),
(22, 'DTMM\r'),
(23, 'EA\r'),
(24, 'EDER\r'),
(25, 'EHS\r'),
(26, 'EID\r'),
(27, 'Engine\r'),
(28, 'FIN\r'),
(29, 'Finance\r'),
(30, 'GA\r'),
(31, 'General Accounting & Tax\r'),
(32, 'HR/HRO 2\r'),
(33, 'HRD\r'),
(34, 'HRO\r'),
(35, 'IARM\r'),
(36, 'Infrastructure\r'),
(37, 'IR\r'),
(38, 'IRA\r'),
(39, 'KAP\r'),
(40, 'KEP\r'),
(41, 'LOG\r'),
(42, 'Log. Assy\r'),
(43, 'Logistic - EID\r'),
(44, 'Logistic Assy\r'),
(45, 'Maintenance\r'),
(46, 'Marketing Product Planning / Product I & II\r'),
(47, 'MPP\r'),
(48, 'MPPD\r'),
(49, 'MTN\r'),
(50, 'PAD\r'),
(51, 'PAINT\r'),
(52, 'Painting 3\r'),
(53, 'PARTS\r'),
(54, 'PCD\r'),
(55, 'PCL\r'),
(56, 'PE\r'),
(57, 'PE / Design Engineering\r'),
(58, 'PE / Project Cost Planning\r'),
(59, 'PE Assy\r'),
(60, 'PE Press\r'),
(61, 'PPCL\r'),
(62, 'PRESS\r'),
(63, 'PRO\r'),
(64, 'PROD\r'),
(65, 'Produksi\r'),
(66, 'Project Cost Planning\r'),
(67, 'PTC\r'),
(68, 'PuD\r'),
(69, 'PURCHASING\r'),
(70, 'QA\r'),
(71, 'QC\r'),
(72, 'QE\r'),
(73, 'QE / QC\r'),
(74, 'QI\r'),
(75, 'QI - QC\r'),
(76, 'Quality\r'),
(77, 'R&D\r'),
(78, 'RnD\r'),
(79, 'RPM\r'),
(80, 'SPD\r'),
(81, 'Technical Service\r'),
(82, 'TSD\r');

-- --------------------------------------------------------

--
-- Table structure for table `printer`
--

CREATE TABLE `printer` (
  `sn` varchar(20) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `lokasi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `printer`
--

INSERT INTO `printer` (`sn`, `nama`, `lokasi`) VALUES
('\"140268', 'HO-Ckd-01\"\r', '');

-- --------------------------------------------------------

--
-- Table structure for table `usage_printer`
--

CREATE TABLE `usage_printer` (
  `id` int(11) NOT NULL,
  `username` int(11) NOT NULL,
  `id_bulan` int(11) NOT NULL,
  `tahun` int(4) NOT NULL,
  `sn_printer` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `sheets` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `nama` varchar(70) NOT NULL,
  `id_divisi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bulan`
--
ALTER TABLE `bulan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `divisi`
--
ALTER TABLE `divisi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `printer`
--
ALTER TABLE `printer`
  ADD PRIMARY KEY (`sn`);

--
-- Indexes for table `usage_printer`
--
ALTER TABLE `usage_printer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`username`),
  ADD KEY `id_bulan` (`id_bulan`),
  ADD KEY `id_tahun` (`tahun`),
  ADD KEY `id_type` (`type`),
  ADD KEY `id_printer` (`sn_printer`),
  ADD KEY `id_tahun_2` (`tahun`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_divisi` (`id_divisi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bulan`
--
ALTER TABLE `bulan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `divisi`
--
ALTER TABLE `divisi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `usage_printer`
--
ALTER TABLE `usage_printer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_divisi`) REFERENCES `divisi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
