<?php include 'header.php';
/*bulan*/ 
$q = "SELECT * FROM bulan";
$r = mysqli_query($conn, $q);
$bulan = '';
while($row = mysqli_fetch_array($r))
{
    $bulan .= "<option value='".$row['id']."'>".$row['bulan']."</option>";
}
/*tahun*/
$q = "SELECT distinct(tahun) as th FROM usage_printer";
$r = mysqli_query($conn, $q);
$tahun = '';
while($row = mysqli_fetch_array($r))
{
    $tahun .= "<option value='".$row['th']."'>".$row['th']."</option>";
}
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content">
                        <div class="form-inline">
                          <div class="form-group">
                            <label> USAGE PRINTER IN     : </label>
                            <select class="form-control" name="bulan" id="bulan">
                                <?=$bulan?>
                            </select>
                            <select class="form-control form-xs" name="tahun" id="tahun">
                                <?=$tahun?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary" id="submit" >show</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">PRINTER USAGE</h4>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="table table-hover table-striped" id="printer">
                        <thead>
                            <th>ID</th>
                            <th>Serial Number</th>
                            <th>Printer Name</th>
                            <th>Location</th>
                            <th>B/W</th>
                            <th>Color</th>
                            <th>Total</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php include 'footer.php'; ?>

<script type="text/javascript">
    $(document).ready(function(){
        $('#submit').click(function () {
            /*top BW*/
            $.ajax({
                url: 'data.php',
                data: { tipe: 4,  
                    bulan: $('#bulan').val(), 
                    tahun: $('#tahun').val() },
                    type: 'get',
                    dataType: 'json'
                }).done(function (data)
                {
                    $('#printer tbody').html(data.data);
                });
            });

    });
</script>