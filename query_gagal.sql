SELECT
printer.sn,
printer.nama,
printer.lokasi,
SUM(usage_printer.sheets) AS bw,
usage_color.color AS color,
sheets+color AS total
FROM usage_printer
JOIN 
(
SELECT sn_printer AS sn, SUM(usage_printer.sheets) AS color
FROM usage_printer
WHERE 
usage_printer.tahun = 2016 AND
usage_printer.id_bulan = 12 AND
usage_printer.type = 1
GROUP BY sn
) AS usage_color
ON usage_printer.sn_printer = usage_color.sn
INNER JOIN printer
ON usage_printer.sn_printer = printer.sn
WHERE
usage_printer.tahun = 2016 AND
usage_printer.id_bulan = 12 AND
usage_printer.type = 0
GROUP BY sn